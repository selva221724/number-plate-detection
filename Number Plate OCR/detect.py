import requests
import base64
import json
import pandas
import numpy as np
import cv2 as cv

aa = 1
import tkinter as tk
from tkinter import filedialog
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()

# Sample image file is available at http://plates.openalpr.com/ea7the.jpg
IMAGE_PATH = file_path
SECRET_KEY = 'sk_be1f2c57085af2cea44ff03d'

with open(IMAGE_PATH, 'rb') as image_file:
    img_base64 = base64.b64encode(image_file.read())

url = 'https://api.openalpr.com/v2/recognize_bytes?recognize_vehicle=1&country=in&secret_key=%s' % (SECRET_KEY)
r = requests.post(url, data = img_base64)
try1=json.dumps(r.json(), indent=2)

print(json.dumps(r.json(), indent=2))
#pandas.read_json(v).to_excel("output.xlsx")

wjdata = json.loads(try1)

for ff in wjdata['results'][0]:
    print(aa)
    print(ff)
    print("\n")
    aa += 1

    
u = wjdata['results'][0]['coordinates']

x1 = wjdata['results'][0]['coordinates'][3]['x']
y1 = wjdata['results'][0]['coordinates'][3]['y']
x2 = wjdata['results'][0]['coordinates'][1]['x']
y2 = wjdata['results'][0]['coordinates'][1]['y']

x3= wjdata['results'][0]['coordinates'][0]['x']
y3= wjdata['results'][0]['coordinates'][0]['y']
x4= wjdata['results'][0]['coordinates'][2]['x']
y4= wjdata['results'][0]['coordinates'][2]['y']

plate = wjdata['results'][0]['plate']
vehicle_orientation = wjdata['results'][0]['vehicle']['orientation'][0]['name']
vehicle_color = wjdata['results'][0]['vehicle']['color'][0]['name']
vehicle_make = wjdata['results'][0]['vehicle']['make'][0]['name']
vehicle_body_type = wjdata['results'][0]['vehicle']['body_type'][0]['name']
vehicle_make_model = wjdata['results'][0]['vehicle']['make_model'][0]['name']



source_img = cv.imread(file_path)
points = np.array([[[x3, y3], [x2, y2], [x4, y4], [x1, y1]]], np.int32)
img = cv.polylines(source_img, [points], True, (0,0,255), thickness=2)


imS = cv.resize(img, (960, 540))

hh = cv.rectangle(imS, (2, 50), (260, 300),(255,255,255),-1)

black = (0, 0, 0)
                  
font = cv.FONT_HERSHEY_PLAIN
img2 = cv.putText(hh, "Plate No:"+plate, (10, 80), font, 1, black, 1)
img3 = cv.putText(img2, "Orientation:"+vehicle_orientation, (10, 120), font, 1, black, 1)
img4 = cv.putText(img3, "Color:"+vehicle_color, (10, 160), font, 1, black, 1)
img5 = cv.putText(img4, "Make:"+vehicle_make, (10, 200), font, 1, black, 1)
img6 = cv.putText(img5, "Body_type:"+vehicle_body_type, (10, 240), font, 1, black, 1)
img7 = cv.putText(img6, "Make_model:"+vehicle_make_model, (10, 280), font, 1, black, 1)


cv.imshow('Draw01',img7)
cv.waitKey(0)



